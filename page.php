<?php get_header(); ?>

<div class="wrapper container" id="page-template">
	<?php if (have_posts()): while (have_posts()) : the_post(); ?>
		<h2 class="page-title"><?php the_title(); ?></h2>
		<?php the_content(); ?>
	<?php endwhile; ?>

	<?php endif; ?>
</div>
<?php get_footer(); ?>
