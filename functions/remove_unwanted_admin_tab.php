<?php
/*------------------------------------*\
    Remove unwanted tab on admin panel
\*------------------------------------*/
function pst_remove_admin_menus()
{
    if (function_exists('remove_menu_page')) {
        remove_menu_page('edit-comments.php' );
        remove_menu_page('index.php' );
        // remove_menu_page('tools.php' );
        // remove_menu_page('upload.php' );
        // remove_menu_page('edit.php' );
        remove_menu_page('users.php' );
        // remove_menu_page('plugins.php' );
        // remove_menu_page('options-general.php' );
    }
}
add_action('admin_menu','pst_remove_admin_menus');

function pst_remove_submenu_elements()
{
    remove_submenu_page( 'themes.php', 'theme-editor.php' );
    remove_submenu_page( 'themes.php', 'customize.php' );
    // remove_submenu_page( 'themes.php', 'widgets.php' );
    remove_submenu_page( 'plugins.php', 'plugin-editor.php' );
}
add_action('admin_init', 'pst_remove_submenu_elements', 102);