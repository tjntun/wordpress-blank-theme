<?php
add_action('wp_enqueue_scripts', 'pst_styles'); // Add Theme Stylesheet
// Load styles
function pst_styles()
{
    // Register
    wp_register_style('main', get_template_directory_uri() . '/css/main.css', array(), '', 'all');
    
    // Enqueue
    wp_enqueue_style('main');
}

add_action( 'admin_enqueue_scripts', 'pst_add_admin_styles' );

function pst_add_admin_styles() {
    /**
     *  Custom post slideshow
     *  uncomment below functions to enqueue to admin
     */
    
    // wp_register_style( 'meta.slideshow', get_template_directory_uri() . '/functions/slideshow-meta-boxes/slideshow.meta.css' );
    // wp_enqueue_style('meta.slideshow' );
}
?>