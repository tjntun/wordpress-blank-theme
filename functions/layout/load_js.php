<?php
add_action('init', 'pst_header_scripts'); // Add Custom Scripts to wp_head
add_action('wp_print_scripts', 'pst_conditional_scripts'); // Add Conditional Page Scripts

// Load JS scripts (header.php)
function pst_header_scripts()
{
    if ($GLOBALS['pagenow'] != 'wp-login.php' && !is_admin()) {
    	wp_register_script('jquery.js', get_template_directory_uri() . '/js/vendor/jquery.js');
	    wp_register_script('plugins.js', get_template_directory_uri() . '/js/plugins.js');
	    wp_register_script('main.js', get_template_directory_uri() . '/js/main.js');

	    wp_enqueue_script( 'jquery.js');
		wp_enqueue_script( 'plugins.js');
		wp_enqueue_script( 'main.js');
    }

    /**
     * Load prettyPhoto plugin for metabox slideshow
     * uncomment below functions to activate
     */
    
    // if (is_admin()) {
    // 	wp_register_script( 'prettyPhoto', get_template_directory_uri() . '/js/vendor/jquery.prettyPhoto.js' );
    // 	wp_enqueue_script('prettyPhoto' );
    // }
}

// Load conditional scripts
function pst_conditional_scripts()
{
   
}