<?php
// Change Post menu label
function change_post_menu_label() {
    global $menu;
    global $submenu;
    $menu[5][0] = 'Tin tức';
    $submenu['edit.php'][5][0] = 'Tin tức';
    $submenu['edit.php'][10][0] = 'Thêm Tin tức';
    $submenu['edit.php'][15][0] = 'Loại tin tức'; // Change name for categories
    $submenu['edit.php'][16][0] = 'Tags'; // Change name for tags
    echo '';
}

function change_post_object_label() {
    global $wp_post_types;
    $labels = &$wp_post_types['post']->labels;
    $labels->name = 'Tin tức';
    $labels->singular_name = 'Tin tức';
    $labels->add_new = 'Thêm Tin tức';
    $labels->add_new_item = 'Thêm Tin tức';
    $labels->edit_item = 'Sửa Tin tức';
    $labels->new_item = 'Tin tức';
    $labels->view_item = 'Xem Tin tức';
    $labels->search_items = 'Tìm Tin tức';
    $labels->not_found = 'Không tìm thấy tin tức';
    $labels->not_found_in_trash = 'Không có Tin tức nào được tìm thấy trong thùng rác';
}
add_action( 'init', 'change_post_object_label' );
add_action( 'admin_menu', 'change_post_menu_label' );