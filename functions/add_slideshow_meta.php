<?php
// The function that outputs the metabox html
function project_slideshow_metabox() {
    global $post;
    // Here we get the current images ids of the gallery
    $values = get_post_custom($post->ID);
    if(isset($values['project_slideshow'])) {
        // The json decode and base64 decode return an array of image ids
        $ids = json_decode(base64_decode($values['project_slideshow'][0]));
    }
    else {
        $ids = array();
    }
    wp_nonce_field('my_meta_box_nonce', 'meta_box_nonce'); // Security
    // Implode the array to a comma separated list
    $cs_ids = implode(",", $ids);   
    
    // We display the gallery
    $html = '<div class="project-slideshow-metabox">';
    $html .= do_shortcode('[gallery size="small" columns="5" ids="'.$cs_ids.'"]');
    $html .= '</div>';

    // Here we store the image ids which are used when saving the product
    $html .= '<input id="project_slideshow_ids" type="hidden" name="project_slideshow_ids" value="'.$cs_ids. '" />';
    // A button which we will bind to later on in JavaScript
    $html .= '<input id="manage_gallery" style="color: #555;
border-color: #ccc;
background: #f7f7f7;
-webkit-box-shadow: inset 0 1px 0 #fff,0 1px 0 rgba(0,0,0,.08);
box-shadow: inset 0 1px 0 #fff,0 1px 0 rgba(0,0,0,.08);border: none; cursor:pointer; padding: 10px 15px;" title="Manage gallery" type="button" value="Manage gallery" />';
    echo $html;
}
 
// A function that will add the metabox to the edit page
function add_project_slideshow_metabox() { 
    // More info about arguments in the WP Codex
    add_meta_box(
        'project_slideshow',          // Name of the box
        __('Gallery'),              // Title of the box
        'project_slideshow_metabox',  // The metabox html function 
        'project',                  // SET TO THE POST TYPE WHERE THE METABOX IS SHOWN
        'normal',                   // Specifies where the box is shown
        'high'                      // Specifies where the box is shown
    ); 
}
 
// This function takes care of saving the metabox's value
function save_product_metaboxes($post_id) {
    // Check if nonce is valid
    if (!isset($_POST['meta_box_nonce'])
        || !wp_verify_nonce($_POST['meta_box_nonce'], 'my_meta_box_nonce'))
        return;
    // Check if user has right access level
    if (!current_user_can('edit_post'))
        return;
 
    // Check if data is in post
    if (isset($_POST['project_slideshow_ids'])) {
        // Encode so it can be stored an retrieved properly
        $encode = base64_encode(json_encode(explode(',',$_POST['project_slideshow_ids'])));
        update_post_meta($post_id, 'project_slideshow', $encode);
    }
}

function register_admin_scripts() {
    wp_register_script('gallery-meta-box', get_template_directory_uri() . '/js/metabox-gallery.js');
    wp_enqueue_script(array('jquery', 'gallery-meta-box'));
}
add_action('admin_enqueue_scripts', 'register_admin_scripts');


// Hook these actions into Wordpress
add_action('add_meta_boxes', 'add_project_slideshow_metabox');
add_action('save_post', 'save_product_metaboxes');