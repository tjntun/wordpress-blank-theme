<?php 
//Adds gallery shortcode defaults of size="full"
function pst_gallery_atts( $out, $pairs, $atts ) {
    $atts = shortcode_atts( array(
      'size' => 'full',
    ), $atts );
 
    $out['size'] = $atts['size'];
 
    return $out;
 
}
add_filter( 'shortcode_atts_gallery', 'pst_gallery_atts', 10, 3 );