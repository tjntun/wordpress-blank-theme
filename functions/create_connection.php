<?php
function pst_connection_types() {
	p2p_register_connection_type(array(
		'name' => 'service_explore_to_project',
		'from' => 'service-explore',
		'to' => 'project'
	));
	p2p_register_connection_type(array(
		'name' => 'service_design_to_project',
		'from' => 'service-design',
		'to' => 'project'
	));
	p2p_register_connection_type(array(
		'name' => 'service_advisory_to_project',
		'from' => 'service-advisory',
		'to' => 'project'
	));
	p2p_register_connection_type(array(
		'name' => 'member_to_project',
		'from' => 'member',
		'to' => 'project'
	));
}

add_action( 'p2p_init', 'pst_connection_types' );
?>