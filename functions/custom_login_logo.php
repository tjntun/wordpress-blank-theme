<?php 
// Custom WordPress Login Logo
function login_css() {
    wp_enqueue_style( 'login_css', get_template_directory_uri() . '/css/login.css' );
}
add_action('login_head', 'login_css');

// Use your own external URL logo link
function pst_url_login(){
    return "http://wonav.com";
}
add_filter('login_headerurl', 'pst_url_login');

function pst_login_logo_url_title() {
    return 'Phát triển bởi Wonav Corp';
}
add_filter( 'login_headertitle', 'pst_login_logo_url_title' );