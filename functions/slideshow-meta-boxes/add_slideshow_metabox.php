<?php
add_action( 'add_meta_boxes', 'add_slideshow_metabox' );
function add_slideshow_metabox() {
	add_meta_box(
		'restaurant_slideshow_meta', // id
		__('Slideshow', 'pst'), // title
		'show_slideshow_meta_box', // callback
		'restaurant', // post type
		'side' // context
	);
}

function show_slideshow_meta_box() {
	global $post;
	include TEMPLATEPATH .'/functions/slideshow-meta-boxes/slideshow_meta.htm.php';
}

function pst_save_meta($post_id) {
	// check autosave
	if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
		return;
	}
	
	// check whether this is an accommodation post type
	if (!isset($_REQUEST['post_type']) || $_REQUEST['post_type'] != 'restaurant') {
		return;
	}
	
	// check permissions
	if (!current_user_can('edit_post', $post_id)) {
		return;
	}
	
	// update slideshow images
	if ( isset($_REQUEST['slideshow_images']) ) {
		$attachment_ids = array_filter(explode(',', sanitize_text_field($_REQUEST['slideshow_images'] )));
		update_post_meta($post_id, '_slideshow_images', implode(',', $attachment_ids));
	}

}

add_action('save_post', 'pst_save_meta', 1, 2);	