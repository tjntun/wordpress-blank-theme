<?php
// Change welcome page to specific page
function loginRedirect( $redirect_to, $request, $user ){
    if( is_array( $user->roles ) ) { // check if user has a role
        return "/wp-admin/themes.php";
    }
}
add_filter("login_redirect", "loginRedirect", 10, 3);