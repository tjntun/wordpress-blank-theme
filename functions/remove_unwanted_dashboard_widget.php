<?php
// remove unwanted dashboard widgets for relevant users
function pst_remove_dashboard_widgets() {
    $user = wp_get_current_user();
    remove_meta_box( 'dashboard_recent_comments', 'dashboard', 'normal' );
    remove_meta_box( 'dashboard_incoming_links', 'dashboard', 'normal' );
    remove_meta_box( 'dashboard_quick_press', 'dashboard', 'side' );
    remove_meta_box( 'dashboard_primary', 'dashboard', 'side' );
    remove_meta_box( 'dashboard_secondary', 'dashboard', 'side' );
    remove_meta_box( 'dashboard_right_now', 'dashboard', 'normal' );
    remove_meta_box( 'dashboard_activity', 'dashboard', 'normal' );
    remove_meta_box( 'wp_welcome_panel', 'dashboard', 'normal' );
}
add_action( 'wp_dashboard_setup', 'pst_remove_dashboard_widgets' );