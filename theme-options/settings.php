<?php

/**
 * pst Tabbed Settings Page
 */

add_action( 'init', 'pst_admin_init' );
add_action( 'admin_menu', 'pst_settings_page_init' );

function pst_admin_init() {
	$settings = get_option( "pst_theme_settings" );
	if ( empty( $settings ) ) {
		$settings = array(
			'pst_intro' => 'Some intro text for the home page',
			'pst_intro_page_id' => false,
			'pst_tag_class' => false,
			'pst_ga' => false
		);
		add_option( "pst_theme_settings", $settings, '', 'yes' );
	}	
}

function pst_settings_page_init() {
	$theme_data = get_theme_data( TEMPLATEPATH . '/style.css' );
	$settings_page = add_theme_page( $theme_data['Name']. ' Theme Settings', $theme_data['Name']. ' Theme Settings', 'edit_theme_options', 'theme-settings', 'pst_settings_page' );
	add_action( "load-{$settings_page}", 'pst_load_settings_page' );
}

function pst_load_settings_page() {
	if ( $_POST["pst-settings-submit"] == 'Y' ) {
		check_admin_referer( "pst-settings-page" );
		pst_save_theme_settings();
		$url_parameters = isset($_GET['tab'])? 'updated=true&tab='.$_GET['tab'] : 'updated=true';
		wp_redirect(admin_url('themes.php?page=theme-settings&'.$url_parameters));
		exit;
	}
}

function pst_save_theme_settings() {
	global $pagenow;
	$settings = get_option( "pst_theme_settings" );
	
	if ( $pagenow == 'themes.php' && $_GET['page'] == 'theme-settings' ){ 
		if ( isset ( $_GET['tab'] ) )
	        $tab = $_GET['tab']; 
	    else
	        $tab = 'homepage'; 

	    switch ( $tab ){ 
	        case 'general' :
				$settings['pst_tag_class']	  = $_POST['pst_tag_class'];
			break; 
	        case 'footer' : 
				$settings['pst_ga']  = $_POST['pst_ga'];
			break;
			case 'homepage' : 
				$settings['pst_intro']	  = $_POST['pst_intro'];
				$settings['pst_intro_page_id']	  = $_POST['pst_intro_page_id'];
			break;
	    }
	}
	
	if( !current_user_can( 'unfiltered_html' ) ){
		if ( $settings['pst_ga']  )
			$settings['pst_ga'] = stripslashes( esc_textarea( wp_filter_post_kses( $settings['pst_ga'] ) ) );
		if ( $settings['pst_intro'] )
			$settings['pst_intro'] = stripslashes( esc_textarea( wp_filter_post_kses( $settings['pst_intro'] ) ) );
	}

	$updated = update_option( "pst_theme_settings", $settings );
}

function pst_admin_tabs( $current = 'homepage' ) { 
    $tabs = array( 'homepage' => 'Home', 'general' => 'General', 'footer' => 'Footer' ); 
    $links = array();
    echo '<div id="icon-themes" class="icon32"><br></div>';
    echo '<h2 class="nav-tab-wrapper">';
    foreach( $tabs as $tab => $name ){
        $class = ( $tab == $current ) ? ' nav-tab-active' : '';
        echo "<a class='nav-tab$class' href='?page=theme-settings&tab=$tab'>$name</a>";
    }
    echo '</h2>';
}

function pst_settings_page() {
	global $pagenow;
	$settings = get_option( "pst_theme_settings" );
	$theme_data = get_theme_data( TEMPLATEPATH . '/style.css' );
	?>
	
	<div class="wrap">
		<h2><?php echo $theme_data['Name']; ?> Theme Settings</h2>
		
		<?php
			if ( 'true' == esc_attr( $_GET['updated'] ) ) echo '<div class="updated" ><p>Theme Settings updated.</p></div>';
			
			if ( isset ( $_GET['tab'] ) ) pst_admin_tabs($_GET['tab']); else pst_admin_tabs('homepage');
		?>

		<div id="poststuff">
			<form method="post" action="<?php admin_url( 'themes.php?page=theme-settings' ); ?>">
				<?php
				wp_nonce_field( "pst-settings-page" ); 
				
				if ( $pagenow == 'themes.php' && $_GET['page'] == 'theme-settings' ){ 
				
					if ( isset ( $_GET['tab'] ) ) $tab = $_GET['tab']; 
					else $tab = 'homepage'; 
					
					echo '<table class="form-table">';
					switch ( $tab ){
						case 'general' :
							?>
							<tr>
								<th><label for="pst_tag_class">Tags with CSS classes:</label></th>
								<td>
									<input id="pst_tag_class" name="pst_tag_class" type="checkbox" <?php if ( $settings["pst_tag_class"] ) echo 'checked="checked"'; ?> value="true" /> 
									<span class="description">Output each post tag with a specific CSS class using its slug.</span>
								</td>
							</tr>
							<?php
						break; 
						case 'footer' : 
							?>
							<tr>
								<th><label for="pst_ga">Insert tracking code:</label></th>
								<td>
									<textarea id="pst_ga" name="pst_ga" cols="60" rows="5"><?php echo esc_html( stripslashes( $settings["pst_ga"] ) ); ?></textarea><br/>
									<span class="description">Enter your Google Analytics tracking code:</span>
								</td>
							</tr>
							<?php
						break;
						case 'homepage' : 
							?>
							<tr>
								<th><label for="pst_intro">Introduction</label></th>
								<td>
									<textarea id="pst_intro" name="pst_intro" cols="60" rows="5" ><?php echo esc_html( stripslashes( $settings["pst_intro"] ) ); ?></textarea><br/>
									<span class="description">Enter the introductory text for the home page:</span>
								</td>
							</tr>
							<tr>
								<th><label for="pst_intro">Trang giới thiệu</label></th>
								<td>
									<?php wp_dropdown_pages(array('name'=>'pst_intro_page_id', 'selected'=> get_option('pst_theme_settings')['pst_intro_page_id'])); ?><br/>
									<span class="description">Lựa chọn trang giới thiệu xuất hiện ở <a target="blank" href="<?php echo home_url() ?>">Trang chủ</a></span>
								</td>
							</tr>
							<?php
						break;
					}
					echo '</table>';
				}
				?>
				<p class="submit" style="clear: both;">
					<input type="submit" name="Submit"  class="button-primary" value="Update Settings" />
					<input type="hidden" name="pst-settings-submit" value="Y" />
				</p>
			</form>
			
			<p><?php echo $theme_data['Name'] ?> theme by Phạm Sỹ Tùng (<a href="http://wonav.com">tungps@wonav.com</a>)</p>
		</div>

	</div>
<?php
}


?>