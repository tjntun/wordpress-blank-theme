<?php get_header(); ?>
<div class="wrapper container" id="single-template">

	<?php if (have_posts()): while (have_posts()) : the_post(); ?>
		<h2 class="page-title"><?php the_title(); ?></h2>
		<span class="post-datetime"><?php the_date(); ?></span>
		<div class="content">
			<?php the_content(); ?>
		</div>
	<?php endwhile; ?>
	<?php endif; ?>

</div>
<?php get_footer(); ?>