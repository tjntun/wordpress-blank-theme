VECC = {
	init: function(){
		this.initSlider();
	},
	initSlider: function () {
		var _self = this;
		try{
			$('.bxslider').bxSlider();
		}catch(e){
			_self.log('Missing bxSlider plugin', e);
		}
	},
	log: function (event, msg) {
		if(console.log){
			console.log(msg);
			console.log('Stack trace:', event);	
		}
	},
	cl: function (msg) {
		if (console.log) {
			console.log(msg);
		}
	}
}

VECC_News = {
	init: function () {
		
	},
	eventPagination: function () {
		
	}
}

jQuery(document).ready(function($) {
	VECC.init();
});	