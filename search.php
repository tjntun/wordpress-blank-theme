<?php get_header(); ?>
<div class="wrapper container" id="search-template">
	<h1 class="page-title"><?php echo sprintf( __( '%s Kết quả tìm kiếm cho từ khóa: ', 'pst' ), $wp_query->found_posts ); echo get_search_query(); ?></h1>
	<?php get_template_part('loop'); ?>
	<?php get_template_part('pagination'); ?>
</div>
<?php get_footer(); ?>
